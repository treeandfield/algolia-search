 angular
	    .module('myapp', ['algoliasearch'])
	    .controller('SearchCtrl', ['$scope', 'algolia', function($scope, algolia) {
	      
	      var client = algolia.Client("7AKCJMOMK0", '32edfffd691294b7668cab87190a6d09');
	      var index = client.initIndex('test_weber');
	      
	      $scope.q = '';
	      $scope.fail = '';
	      
	      $scope.hits = [];
	      
	      $scope.k = function(){
		        index.search($scope.q, {
		       attributesToRetrieve: ['doc'],
		       hitsPerPage: 1000
	         })
		        .then(function searchSuccess(content) {
		           $scope.hits = content.hits;
		        }, function searchFailure(err) {
		           $scope.fail = err;
		        });
	      };
	      
	      index.search($scope.q, {
		      attributesToRetrieve: ['doc'],
		      hitsPerPage: 1000
	      })
	      .then(function searchSuccess(content) {
		        $scope.hits = content.hits;
		   }, function searchFailure(err) {
		        $scope.fail = err;
		   });
	      

	    }]);